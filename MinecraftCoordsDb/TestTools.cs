﻿using System;
using System.Collections.Generic;

namespace MinecraftCoordsDb
{
    public static class TestTools
    {
        static string[] Realms = { "End", "Nether", "Overworld" };
        static Random rand = new Random();

        public static List<CoordInfo> TestData(int count = 1)
        {
            var result = new List<CoordInfo>();
            for (int i = 0; i < count; i++)
            {
                result.Add(new CoordInfo
                {
                    Coords = "1/2/3",
                    Info = "SHIP",
                    IsLooted = rand.Next(0, 100) > 50 ? true : false,
                    Name = "Portal",
                    Realm = Realms[rand.Next(0, 3)]
                });
            }

            return result;
        }
    }
}
