﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MinecraftCoordsDb
{
    public class DataHandler
    {
        private const string DbName = @"MinecraftCoords.db";
        private const string CollectionName = "coordInfoList";

        private List<CoordInfo> coordInfoList = new List<CoordInfo>();
        private bool isTest;

        public DataHandler(bool isTest = false)
        {
            this.isTest = isTest;
        }

        public void AssignData(DataGrid dataGrid)
        {
            if (this.isTest)
            {
                int testDataCount = 10;

                coordInfoList = TestTools.TestData(testDataCount);
                dataGrid.ItemsSource = coordInfoList;

                return;
            }

            this.coordInfoList = GetDataFromDatabase();
            dataGrid.ItemsSource = this.coordInfoList;
        }

        public void AddCoords(DataGrid dataGrid, CoordInfo coordInfo)
        {
            coordInfoList.Add(coordInfo);
            dataGrid.ItemsSource = null;
            dataGrid.ItemsSource = coordInfoList;

            using (var db = new LiteDatabase(DataHandler.DbName))
            {
                var coordInfoList = db.GetCollection<CoordInfo>(DataHandler.CollectionName);

                coordInfoList.Insert(coordInfo);
            }

            if (this.isTest)
                return;
        }

        private List<CoordInfo> GetDataFromDatabase()
        {
            var dbContents = new List<CoordInfo>();
            using (var db = new LiteDatabase(DataHandler.DbName))
            {
                var coordInfoList = db.GetCollection<CoordInfo>(DataHandler.CollectionName);

                if (coordInfoList != null)
                    dbContents = coordInfoList.FindAll().ToList();
            }

            return dbContents;
        }
    }
}
