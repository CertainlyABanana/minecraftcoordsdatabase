﻿using LiteDB;
using System;

namespace MinecraftCoordsDb
{
    public class CoordInfo
    {
        [BsonId]
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public string Coords { get; set; }
        public string Realm { get; set; }
        public bool IsLooted { get; set; }
        public string Info { get; set; }
    }
}
