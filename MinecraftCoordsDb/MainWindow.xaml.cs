﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MinecraftCoordsDb
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataHandler dataHandler = new DataHandler();

        public MainWindow()
        {
            this.InitializeComponent();
            dataHandler.AssignData(coordsList);
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            debugBox.Text = "Add button clicked";

            CoordInfo coordInfo = new CoordInfo()
            {
                Coords = coordsAdd.Text,
                Info = infoAdd.Text,
                IsLooted = (bool)lootedAdd.IsChecked,
                Name = nameAdd.Text,
                Realm = realmAdd.SelectionBoxItem.ToString()
            };

            dataHandler.AddCoords(coordsList, coordInfo);
        }
    }
}
